# README #

Beginning of Unreal

Elevator Pitch:
You are a mercenary in a futuristic world and need to steal a ship to escape. Find your way to the top of the building to leave

Unreal Postmortem 
I created a large city block with skyscrapers, a park, and lots of cars on the road. The main objective is to get from your building to the enemy's corporation headquarters and steal their spaceship. It’s a first-person action game set in a futuristic society.
One of the main technical things I worked with was the geometry editor. I created the starting apartment building to have multiple levels and a glass wall, so the player sees a vague layout of the city. I started by just creating one box and using subtraction boxes to create the levels of the building. I applied the same techniques to the corporation building as well. The building is shaped like an H and is hollowed out with levels on both sides. The other main technical aspect was I designed the materials for the buildings. I created them using the base color, specular, and roughness tools in the material live editor screen. I then made those into parameters so I can create new instances of the material easily. 
To design the feel of the level I used the post processing volume to give the game a sleek tint over the level. The next design piece was creating the pathway to the final building. The first change was I made the material on the final building brighter. I then customized the directional lighting to make sure it was shining directly on the building. This also caused shadows to cover the sections of the map I didn’t want players to walk through. The next thing I did to highlight sections was make small breaks in the cars on the ground. This makes it look like they are leading you into the park and then across the street to the final building. 
One of the main things I would like to improve on is the lighting effects. I feel like I did not utilize nearly enough to draw players' eyes. I would also go back through and make more buildings that you can walk through to create more options for the player. I think it takes away from the freedom of the level that you can’t go in or explore more. The last thing I would fix is the bounds of the map. If the player looks down the road, he can clearly see that it just stops. I would add more buildings and some roads to finish off the corners of the level. 
